const express = require('express') // inisiasi variable yang berisi express
const router = require('./router')
const app = express() // inisiasi function express ke variable app
require('dotenv').config() // kegunaan untuk membaca file .env
app.use(express.urlencoded({ extended: false })) // berguna untuk menerima request form-data dan urlencode form
app.use(express.json());
app.use(express.static("public"));
app.use("/static", express.static("public"));
app.set("views", "views");
app.set("view engine", "hbs");
//app.set('view engine', 'ejs')
const hbs = require("hbs");
hbs.registerHelper("selected", function (a, b, options) {
    if (a.toLowerCase() === b.toLowerCase())
      return new hbs.SafeString(`selected`);
  });
const port = process.env.PORT || 3500 // declare port dr env || 3500
const { options } = require("./router");
const cors = require('cors') // inisiasi variable yang berisi cors
const bodyParser = require('body-parser') // inisiasi variable yang berisi body parser

app.use(cors()) // gunakan fungsi cors
app.use(bodyParser.json()) // gunakan fungsi json dari body parser (berguna untuk menangkap json request)
app.use('/', router)

app.listen(port, () => {
    console.log(`server running at port ${port}`)
}) // fungsi untuk inisiasi http server pada port yang telah di tentukan
