const axios = require("axios");

exports.index = (req, res) => {
  axios
    .get("http://localhost:3000/api/cars")
    .then((resp) => {
      const data = { cars: resp.data, name: "Ananda P" };
      res.render("index", data);
    })
    .catch((error) => {
      console.log(error);
    });
};

exports.add_cars = (req, res) => {
  res.render("add_cars");
};

exports.edit_cars = (req, res) => {
  const url = `http://localhost:3000/api/car/${req.query.id}`;
  axios
    .get(url)
    .then((resp) => {
      const { id, name, price, size, image } = resp.data;
      const data = {
        id,
        name,
        price,
        size,
        image,
      };
      res.render("edit_cars", data);
    })
    .catch((error) => {
      console.log(error);
    });
};
