const { rental } = require("../models"); // inisiasi variable model yang berisi model dari folder models

// Retrieve all users from the database
exports.list = (req, res) => {
  rental.findAll().then((car) => {
    res.status(200).json(car);
  });
};

// Create and Save a new datas
exports.create = (req, res) => {
  rental.create({
    name: req.body.name,
    price: req.body.price,
    size: req.body.size,
    image: req.body.image,
  }).then((a) => {
    res.status(200).redirect("/");
  });
};

// Delete a user with the specified id in the request
exports.delete = (req, res) => {
  rental.destroy({
    where: {
      id: req.params.id,
    },
  }).then(() => {
    res.status(200).redirect("/");
  });
};

// Find a single data with an id
exports.get = (req, res) => {
  rental.findOne({
    where: {
      id: req.params.id,
    },
  }).then((car) => {
    res.status(200).json(car);
  });
};

// Update a data by the id in the request
exports.update = (req, res) => {
  const query = {
    where: {
      id: req.params.id,
    },
  };
  rental.update(
    {
      name: req.body.name,
      price: req.body.price,
      size: req.body.size,
      image: req.body.image,
    },
    query
  )
    .then(() => {
      res.status(200).redirect("/");
    })
    .catch((err) => {
      res.status(400).send("error!");
    });
};

exports.filter = (req, res) => {
  const query = {
    where: {
      size: req.params.size,
    },
  };

  // console.log(query);
  rental.findAll(query).then((cars) => res.status(200).json(cars));
};
