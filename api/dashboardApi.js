const express = require("express");
const router = express.Router();
const controllers = require("../controllers/dashboardController");

router.get("/", controllers.index);
router.get("/add", controllers.add_cars);
router.get("/update", controllers.edit_cars);

module.exports = router;
