# Challenge_Chapter_5_Ananda_Prasetya

get("/cars")
get("/car/:id")
post("/update/:id")
post("/car")
get("/delete/:id")
get("/filter/:size")

get("/")
get("/add")
get("/update")

#Link ERD:
![ERD](/public/images/Chapter_5_Ananda_Prasetya.png?raw=true "ERD")