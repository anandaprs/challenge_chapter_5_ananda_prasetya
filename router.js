const express = require("express"); // inisiasi variable yang berisi express
const router = express.Router();

const dashboard = require("./api/dashboardApi");
const carsApi = require("./api/carsApi");

router.use("/", dashboard);
router.use("/api", carsApi);

module.exports = router;
